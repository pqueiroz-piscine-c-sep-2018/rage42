#
#
# This loads the exercises for d03

function rage_do_correction {
    RAGE_EXPECTED_FILE=
    RAGE_EXPECTED_FUNCTION=
    RAGE_EXPECTED_OUTPUT=""
    RAGE_RUN=""
    RAGE_EXPECTED_RETURN_CODE=0
    ft_putchar=false

    case $RAGE_CORRECTION_INDEX in
        0|"0" )
            RAGE_EXPECTED_FUNCTION="eval_expr"
            RAGE_EXPECTED_FILE="Makefile"
            RAGE_RUN="https://gitlab.com/pqueiroz-piscine-c-sep-2018/rage42/snippets/1760644/raw"
            ;;
        * )
            print $White "No auto-check for ${RAGE_CORRECTION_INDEX}, skipping...\n"
            RAGE_CORRECTION_INDEX=$(( RAGE_CORRECTION_INDEX + 1 ))
            return
            ;;
    esac

    print $Cyan "Correcting ex ${RAGE_CORRECTION_INDEX}\n"

    #
    # If the required file does not exist
    if [[ ! -f "$RAGE_EXPECTED_FILE" ]]; then
        echo "Required file '$RAGE_EXPECTED_FILE' doesn't exist"
        echo `pwd`
        exit 1
    fi

    local rage_current=`pwd`

    #
    # Create the tmp folder, copy the files, and cd into it
    mkdir -p "/tmp/rage/${d}/ex${RAGE_CORRECTION_INDEX}"
    cp -r ./* "/tmp/rage/${d}/ex${RAGE_CORRECTION_INDEX}/"
    cd "/tmp/rage/${d}/ex${RAGE_CORRECTION_INDEX}"

    local inc_files="$RAGE_EXPECTED_FILE"

    if [[ $RAGE_RUN == "" ]]; then
        cd $rage_current
        print $White "No auto-tests for ex ${RAGE_CORRECTION_INDEX}, skipping...\n"
        RAGE_CORRECTION_INDEX=$(( RAGE_CORRECTION_INDEX + 1 ))
        return
    fi

    curl -s -o run.sh $RAGE_RUN

    sh run.sh

	if [ $? -ne 0 ]; then
		print $White "Test exited with non-zero code\n"
		print $Red "Fail"
	fi

    # ls "/tmp/rage/${d}/ex${RAGE_CORRECTION_INDEX}"
    print $Green "Compile and run successful, ex${RAGE_CORRECTION_INDEX} OK!\n"

    cd $rage_current

    RAGE_CORRECTION_INDEX=$(( RAGE_CORRECTION_INDEX + 1 ))
}

RAGE_FIRST=false
RAGE_LAST=
RAGE_CORRECTION_INDEX=
RAGE_EXERCISES=()
compile_flags="-Wall -Wextra -Werror $FLAGS"

for i in {0..0}; do

    if [[ $RAGE_FIRST == false ]]; then
        RAGE_FIRST="$i"
    fi

    RAGE_LAST="$i"

    name="ex${i}"
    if [[ $i -le 9 ]]; then name="ex0${i}"; fi
    RAGE_EXERCISES[$i]="$name"

done

RAGE_CORRECTION_INDEX="$RAGE_FIRST"

mkdir -p "/tmp/rage/d${d}"
rm -rf "/tmp/rage/d${d}"
