#
#
# This loads the exercises for d03

function rage_do_correction {
    RAGE_EXPECTED_FILE=
    RAGE_EXPECTED_FUNCTION=
    RAGE_EXPECTED_OUTPUT=""
    RAGE_RUN=""
    RAGE_EXPECTED_RETURN_CODE=0
    ft_putchar=false

    case $RAGE_CORRECTION_INDEX in
        0|"0" )
            RAGE_EXPECTED_FUNCTION="ft_ft"
            RAGE_EXPECTED_FILE="ft_ft.c"
            RAGE_RUN="https://gitlab.com/snippets/1756023/raw"
            ;;
        1|"1" )
            RAGE_EXPECTED_FUNCTION="ft_ultimate_ft"
            RAGE_EXPECTED_FILE="ft_ultimate_ft.c"
            RAGE_RUN="https://gitlab.com/snippets/1756027/raw"
            ;;
        2|"2" )
            RAGE_EXPECTED_FUNCTION="ft_swap"
            RAGE_EXPECTED_FILE="ft_swap.c"
            RAGE_RUN="https://gitlab.com/snippets/1756030/raw"
            ;;
        3|"3" )
            RAGE_EXPECTED_FUNCTION="ft_div_mod"
            RAGE_EXPECTED_FILE="ft_div_mod.c"
            RAGE_RUN="https://gitlab.com/raggesilver/rage42/snippets/1756126/raw"
            ;;
        4|"4" )
            RAGE_EXPECTED_FUNCTION="ft_ultimate_div_mod"
            RAGE_EXPECTED_FILE="ft_ultimate_div_mod.c"
            RAGE_RUN="https://gitlab.com/raggesilver/rage42/snippets/1756128/raw"
            ;;
        5|"5" )
            RAGE_EXPECTED_FUNCTION="ft_putstr"
            RAGE_EXPECTED_FILE="ft_putstr.c"
            RAGE_RUN="https://gitlab.com/snippets/1756122/raw"
            RAGE_EXPECTED_OUTPUT="la batata es the best food in the world"
            ft_putchar=true
            ;;
        6|"6" )
            RAGE_EXPECTED_FUNCTION="ft_strlen"
            RAGE_EXPECTED_FILE="ft_strlen.c"
            RAGE_RUN="https://gitlab.com/snippets/1756124/raw"
            ;;
        7|"7" )
            RAGE_EXPECTED_FUNCTION="ft_strrev"
            RAGE_EXPECTED_FILE="ft_strrev.c"
            RAGE_RUN="https://gitlab.com/snippets/1756127/raw"
            ;;
        8|"8" )
            RAGE_EXPECTED_FUNCTION="ft_atoi"
            RAGE_EXPECTED_FILE="ft_atoi.c"
            RAGE_RUN="https://gitlab.com/snippets/1756129/raw"
            ;;
        9|"9" )
            RAGE_EXPECTED_FUNCTION="ft_sort_integer_table"
            RAGE_EXPECTED_FILE="ft_sort_integer_table.c"
            RAGE_RUN="https://gitlab.com/raggesilver/rage42/snippets/1756132/raw"
            ;;
        * )
            print $White "No auto-check for ${RAGE_CORRECTION_INDEX}, skipping...\n"
            RAGE_CORRECTION_INDEX=$(( RAGE_CORRECTION_INDEX + 1 ))
            return
            ;;
    esac

    print $Cyan "Correcting ex ${RAGE_CORRECTION_INDEX}\n"

    #
    # If the required file does not exist
    if [[ ! -f "$RAGE_EXPECTED_FILE" ]]; then
        echo "Required file '$RAGE_EXPECTED_FILE' doesn't exist"
        echo `pwd`
        exit 1
    fi

    local rage_current=`pwd`

    #
    # Create the tmp folder, copy the files, and cd into it
    mkdir -p "/tmp/rage/${d}/ex${RAGE_CORRECTION_INDEX}"
    cp -r ./* "/tmp/rage/${d}/ex${RAGE_CORRECTION_INDEX}/"
    cd "/tmp/rage/${d}/ex${RAGE_CORRECTION_INDEX}"

    local inc_files="$RAGE_EXPECTED_FILE"

    if [[ $RAGE_RUN == "" ]]; then
        cd $rage_current
        print $White "No auto-tests for ex ${RAGE_CORRECTION_INDEX}, skipping...\n"
        RAGE_CORRECTION_INDEX=$(( RAGE_CORRECTION_INDEX + 1 ))
        return
    fi

    if $ft_putchar; then
        inc_files="${inc_files} ft_putchar.c"
        curl -s -o ft_putchar.c https://gitlab.com/snippets/1756020/raw
    fi

    inc_files="${inc_files} main_print.c"
    curl -s -o main_print.c https://gitlab.com/snippets/1756021/raw

    curl -s -o run.h $RAGE_RUN

    #
    # Compile the program
    print $Cyan "Compiling"
    echo " gcc -o a.out $inc_files $compile_flags"
    gcc -o "a.out" $inc_files $compile_flags
    local out=$?

    #
    # If compiling failed
    if [[ $out -ne 0 ]]; then
        print $Red "Compiling exited with non-zero code ($out)\n"
        exit 1
    fi

    local _output=`/tmp/rage/${d}/ex${RAGE_CORRECTION_INDEX}/a.out`
    out=$?

    #
    # If running failed
    if [[ $out -ne $RAGE_EXPECTED_RETURN_CODE ]]; then
        print $Red "Running exited with non-expected code ($out)\n"
        exit 1
    fi

    echo "Output =="
    echo $_output | cat -e
    echo "Expected =="
    echo $RAGE_EXPECTED_OUTPUT | cat -e

    #
    # If output is not the expedted
    if [[ "$_output" != "$RAGE_EXPECTED_OUTPUT" ]]; then
        print $Red "Running outputed an unexpected result\n"
        exit 1
    fi

    # ls "/tmp/rage/${d}/ex${RAGE_CORRECTION_INDEX}"
    print $Green "Compile and run successful, ex ${RAGE_CORRECTION_INDEX} OK!\n"

    cd $rage_current

    RAGE_CORRECTION_INDEX=$(( RAGE_CORRECTION_INDEX + 1 ))
}

RAGE_FIRST=false
RAGE_LAST=
RAGE_CORRECTION_INDEX=
RAGE_EXERCISES=()
compile_flags="-Wall -Wextra -Werror $FLAGS"

for i in {0..9}; do

    if [[ $RAGE_FIRST == false ]]; then
        RAGE_FIRST="$i"
    fi

    RAGE_LAST="$i"

    name="ex${i}"
    if [[ $i -le 9 ]]; then name="ex0${i}"; fi
    RAGE_EXERCISES[$i]="$name"

done

RAGE_CORRECTION_INDEX="$RAGE_FIRST"

mkdir -p "/tmp/rage/d${d}"
rm -rf "/tmp/rage/d${d}"
